﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVVM.ViewModel;

namespace MultiBoxes
{
    public class BoxItem : ViewModelBase
    {
        private Brush myColor;
        public Brush MyColor
        {
            get { return myColor; }
            set {
                myColor = value;
                OnPropertyChanged();
            }
        }

        private double myWidth;
        public double MyWidth
        {
            get { return myWidth; }
            set {
                myWidth = value;
                OnPropertyChanged();
            }
        }

        private string myMsg;
        public string MyMsg
        {
            get { return myMsg; }
            set { myMsg = value; }
        }
    }

    public class Alarm : ViewModelBase
    {
        private BoxItem[] alarms;
        public BoxItem[] Alarms
        {
            get { return alarms; }
            set {
                alarms = value;
                OnPropertyChanged();
            }
        }
        private string alarmName;
        public string AlarmName
        {
            get { return alarmName; }
            set
            {
                alarmName = value;
                OnPropertyChanged();
            }
        }
        private DateTime alarmDate;
        public DateTime AlarmDate
        {
            get { return alarmDate; }
            set
            {
                alarmDate = value;
                OnPropertyChanged();
            }
        }
    }

    public class MainViewModel : ViewModelBase
    {
        private BoxItem[] testItems;
        public BoxItem[] TestItems
        {
            get { return testItems; }
            set { testItems = value;
                OnPropertyChanged(); }
        }
        private Alarm[] testDataGrid;
        public Alarm[] TestDataGrid
        {
            get { return testDataGrid; }
            set { testDataGrid = value;
                OnPropertyChanged(); }
        }

        public MainViewModel()
        {
            TestItems = new BoxItem[3] {
                new BoxItem { MyColor = Brushes.Red, MyWidth=100, MyMsg="Alarm" },
                new BoxItem { MyColor = Brushes.Yellow, MyWidth=120, MyMsg="woo" },
                new BoxItem { MyColor = Brushes.SeaShell, MyWidth=90, MyMsg="water" } };
            TestDataGrid = new Alarm[3]
            {
                new Alarm { AlarmName="SBomb", AlarmDate=new DateTime(2017,10,8), Alarms =  new BoxItem[3] {
                new BoxItem { MyColor = Brushes.Blue, MyWidth=60, MyMsg="ABC" },
                new BoxItem { MyColor = Brushes.Red, MyWidth=60, MyMsg="Go!!!" },
                new BoxItem { MyColor = Brushes.Green, MyWidth=60, MyMsg="OK?" } } },

                new Alarm { AlarmName="Winter", AlarmDate=new DateTime(2017,3,1), Alarms =  new BoxItem[4] {
                new BoxItem { MyColor = Brushes.SteelBlue, MyWidth=100, MyMsg="steeling" },
                new BoxItem { MyColor = Brushes.Snow, MyWidth=60, MyMsg="snowing" },
                new BoxItem { MyColor = Brushes.Teal, MyWidth=60, MyMsg="ko tech" } ,
                new BoxItem { MyColor = Brushes.Wheat, MyWidth=60, MyMsg="BSTK" } } },

                new Alarm { AlarmName="Rush", AlarmDate=new DateTime(2016,5,12), Alarms =  new BoxItem[2] {
                new BoxItem { MyColor = Brushes.YellowGreen, MyWidth=60, MyMsg="Hola" },
                new BoxItem { MyColor = Brushes.Turquoise, MyWidth=60, MyMsg="Go!!!" }} }
            };
        }
    }
}
