** StackPanel with ItemsSource**

*Now you have a bindable ItemsSource property on this Custom StackPanel.*

---

## Usage

You can find a full WPF demo app inside the project.

You need to import the namespace

```XML
xmlns:mypanel="clr-namespace:MultiBoxesLib;assembly=MultiBoxesLib"
```

An example of XAML binding

```XML
<mypanel:BindableStackPanel 
    ItemsSource="{Binding TestItems}" BoxColorProp="MyColor" BoxWidthProp="MyWidth" BoxTextProp="MyMsg"
    Orientation="Horizontal" Height="100" VerticalAlignment="Bottom" />
```