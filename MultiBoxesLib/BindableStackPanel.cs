﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MultiBoxesLib
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:MultiBoxesLib"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:MultiBoxesLib;assembly=MultiBoxesLib"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:CustomControl1/>
    ///
    /// </summary>
    public class BindableStackPanel : StackPanel
    {
        static BindableStackPanel()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(BindableStackPanel), new FrameworkPropertyMetadata(typeof(BindableStackPanel)));

        }

        public static readonly DependencyProperty BoxColorPropProperty =
            DependencyProperty.RegisterAttached("BoxColorProp", typeof(string), typeof(BindableStackPanel));
        public static void SetBoxColorProp(BindableStackPanel bsp, string prop)
        {
            bsp.SetValue(BoxColorPropProperty, prop);
        }
        public static string GetBoxColorProp(BindableStackPanel bsp)
        {
            return (string)bsp.GetValue(BoxColorPropProperty);
        }

        public static readonly DependencyProperty BoxWidthPropProperty =
            DependencyProperty.RegisterAttached("BoxWidthProp", typeof(string), typeof(BindableStackPanel));
        public static void SetBoxWidthProp(BindableStackPanel bsp, string prop)
        {
            bsp.SetValue(BoxWidthPropProperty, prop);
        }
        public static string GetBoxWidthProp(BindableStackPanel bsp)
        {
            return (string)bsp.GetValue(BoxWidthPropProperty);
        }

        public static readonly DependencyProperty BoxTextPropProperty =
            DependencyProperty.RegisterAttached("BoxTextProp", typeof(string), typeof(BindableStackPanel));
        public static void SetBoxTextProp(BindableStackPanel bsp, string prop)
        {
            bsp.SetValue(BoxTextPropProperty, prop);
        }
        public static string GetBoxTextProp(BindableStackPanel bsp)
        {
            return (string)bsp.GetValue(BoxTextPropProperty);
        }

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.RegisterAttached("ItemsSource", typeof(IEnumerable<object>), typeof(BindableStackPanel), new PropertyMetadata(null, ItemsSourceChanged));

        public static void SetItemsSource(BindableStackPanel bsp, IEnumerable<object> items)
        {
            bsp.SetValue(ItemsSourceProperty, items);
        }
        public static IEnumerable<object> GetItemsSource(BindableStackPanel bsp)
        {
            return (IEnumerable<object>)bsp.GetValue(ItemsSourceProperty);
        }

        private static void ItemsSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BindableStackPanel bsp = d as BindableStackPanel;
            if (bsp != null)
            {
                if (e.NewValue is IEnumerable<object> items)
                {
                    StackPanel sp = bsp as StackPanel;
                    sp.Children.Clear();
                    foreach (object dc in items)
                    {
                        Grid grid = new Grid();
                        grid.DataContext = dc;
                        TextBlock tb = new TextBlock();
                        tb.HorizontalAlignment = HorizontalAlignment.Center;
                        tb.VerticalAlignment = VerticalAlignment.Center;
                        Binding myColorBinding = new Binding(GetBoxColorProp(bsp));
                        Binding myWidthBinding = new Binding(GetBoxWidthProp(bsp));
                        Binding myTextBinding = new Binding(GetBoxTextProp(bsp));
                        grid.Children.Add(tb);
                        grid.SetBinding(BackgroundProperty, myColorBinding);
                        grid.SetBinding(WidthProperty, myWidthBinding);
                        tb.SetBinding(TextBlock.TextProperty, myTextBinding);
                        sp.Children.Add(grid);
                    }
                }

            }
        }

    }
}
